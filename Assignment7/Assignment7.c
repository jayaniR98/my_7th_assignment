//1. Write a C program to reverse a sentence entered by user.
/*#include<stdio.h>
int main()
{
    char sentence[30];
    printf("Reverse of the sentence:");
    scanf("%s",&sentence);
    int i,j;
    for(i=0,j=sentence.length-1;i<j;i++,j--)
    {
        int temp=sentence[j];
        sentence[j]=sentence[i];
        sentence[i]=temp;
      printf("%s",&sentence);
    }
    return 0;
}
*/

#include <stdio.h>
int main()
{
  char ar[100],rev[100];
  int i,j,count=0;
  printf("Enter the sentence:");
  scanf("%s",ar);

  //length of the string
  while(ar[count] != '\0')
  {
    count++;
  }
  j=count-1;  //because array is starting from zero

  //reversing the string by swapping
  for(i=0;i<count;i++)
  {
    rev[i]=ar[j];
    j--;
  }
  printf("\n Reverse of the sentence: %s", rev);
}


